# Heirarchical Modelling Example in Unity3d
##### By Lachlan Stevens (u5826298)
---
#### How do I run this?
In the main directory there is a folder called "Builds", which contains an executable of the scene.

#### What if I want to run it in the Unity3d editor?
On the project seletion screen there is the option to "Open" a project in the top-right corner. Select the "Hierarchical Modelling Example" which you get from this repo. Then, once the project has imported, Navigates to the assets folder in the Unity3d project browser and open the scene "MainScene".

#### How do I view your code?
The code can be viewed without the Unity3d editor simply by opening up "ArmExample.cs" in the editor of your choice.