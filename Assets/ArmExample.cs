﻿using UnityEngine;
using UnityEngine.UI;

public class ArmExample : MonoBehaviour {

    [SerializeField] private Slider     Shld_Slider;
    [SerializeField] private Slider     Elbw_Slider;
    [SerializeField] private Slider     Wrst_Slider;

    [SerializeField] private Material   Joint;
    [SerializeField] private Material   Limb;

    private GameObject Shld; // Shoulder Joint
    private GameObject Elbw; // Elbow Joint
    private GameObject Wrst; // Wrist Joint

    private GameObject Uppr; // Upper Arm Limb (Humerus)
    private GameObject Frrm; // Forearm Limb (Radius/Ulna)
    private GameObject Hand; // Hand Limb

    /// <summary>
    /// Initialization Code
    /// </summary>
    void Start () {

        CreateObjects();

        GenerateHierarchy();

        LayoutObjects();

    }
	
    /// <summary>
    /// Code ran every frame
    /// </summary>
	void Update () {
        // Calculate the rotations of each join given each slider every frame.
        // Must use localEulerAngles because localRotation works in quaternions, which aren't
        // too intuitive.
        Shld.transform.localEulerAngles = new Vector3(0, 0, Shld_Slider.value);
        Elbw.transform.localEulerAngles = new Vector3(0, 0, Elbw_Slider.value);
        Wrst.transform.localEulerAngles = new Vector3(0, 0, Wrst_Slider.value);
    }

    /// <summary>
    /// Create The rig objects
    /// </summary>
    private void CreateObjects()
    {
        Shld = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        Shld.name = "Shoulder Joint";
        Shld.GetComponent<MeshRenderer>().material = Joint;

        Elbw = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        Elbw.name = "Elbow Joint";
        Elbw.GetComponent<MeshRenderer>().material = Joint;

        Wrst = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        Wrst.name = "Wrist Joint";
        Wrst.GetComponent<MeshRenderer>().material = Joint;

        Uppr = GameObject.CreatePrimitive(PrimitiveType.Cube);
        Uppr.name = "Upper Arm";
        Uppr.GetComponent<MeshRenderer>().material = Limb;

        Frrm = GameObject.CreatePrimitive(PrimitiveType.Cube);
        Frrm.name = "Forearm";
        Frrm.GetComponent<MeshRenderer>().material = Limb;

        Hand = GameObject.CreatePrimitive(PrimitiveType.Cube);
        Hand.name = "Hand";
        Hand.GetComponent<MeshRenderer>().material = Limb;
    }

    /// <summary>
    /// Create the object hierarchy.
    /// Shoulder > Upper Arm > Elbow > Forearm > Wrist > Hand
    /// </summary>
    private void GenerateHierarchy()
    {
        Shld.transform.parent = transform;          // Add base object as parent to shoulder
        Uppr.transform.parent = Shld.transform;     // The upper arm is connected to the shoulder
        Elbw.transform.parent = Uppr.transform;     // The elbow is connected to the upper arm
        Frrm.transform.parent = Elbw.transform;     // The forearm is connected to the elbow
        Wrst.transform.parent = Frrm.transform;     // The wrist is connected to the forearm
        Hand.transform.parent = Wrst.transform;     // The hand is connected to the wrist
    }

    /// <summary>
    /// Move each game object to the correct relative position and layout
    /// </summary>
    private void LayoutObjects()
    {
        Shld.transform.localPosition = new Vector3(0, 0, 0);
        Uppr.transform.localPosition = new Vector3(1, 0, 0);
        Elbw.transform.localPosition = new Vector3(1, 0, 0);
        Frrm.transform.localPosition = new Vector3(1, 0, 0);
        Wrst.transform.localPosition = new Vector3(1, 0, 0);
        Hand.transform.localPosition = new Vector3(1, 0, 0);
    }
}
